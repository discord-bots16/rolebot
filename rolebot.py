import os
import discord
from dotenv import load_dotenv
from discord.ext import commands
import db_tools
import DiscordUtils
import math

# To use fetch_members(), we need to set the members intent.
intents = discord.Intents.default()
intents.members = True

# Load Environment Variables and other properties
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
BOT_VERSION = '1.2'
BOT_COLOR=0xd53600

# Set prefix
def GetPrefix(bot, message):
	prefixes=['role;']
	return commands.when_mentioned_or(*prefixes)(bot, message)

bot=commands.Bot(command_prefix=GetPrefix, intents=intents)

# Remove default Help command so we can use our own instead
bot.remove_command('help')

# When the bot is logged in and ready, let me know in the console
@bot.event
async def on_ready():
	await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=f'for \'role;\' mentions. | RoleBot v{BOT_VERSION}'))
	print(f'\nLogged in as: {bot.user.name} - {bot.user.id}\nVersion: {BOT_VERSION}\n')
	print(f'I await your command.')

# The help command
@bot.command(name='help', help='Shows this menu')
async def Help(ctx):
	response=discord.Embed(title='RoleBot Help', description=f'I am a fairly primitive bot (Version {BOT_VERSION}, currently), whose sole purpose is to count and display the number of users with each role. I am still under development, so just let @Cliff#4877 know if you would like some behavior changed, or something added.\n\nIf you would like to see it, my source code is on [GitLab](https://gitlab.com/CliffJameston/rolebot).\n\nMy prefix is currently "role;", and these are all the commands I currently will respond to. I will also respond if you just @ me instead.', color=BOT_COLOR)
	response.add_field(name='role;show <role>', value='Shows the number of users with a given role. This uses a fuzzy search, however it is far more reliable if you just type the full name of the role, surrounded by quotes. Currently, pinging the role will *not* get the correct stats, so omit the @ for now.', inline=False)
	response.add_field(name='role;showAll', value='Shows the number of users with each role in the server.', inline=False)
	response.add_field(name='role;countRoles', value='The first command that you should run, this command actually gets every user in the server and tallies all their roles. Caution: depending on the size of the server, this command may take some time. I will let you know when this finishes running.', inline=False)
	response.add_field(name='role;ping', value='Pings me.', inline=False)
	response.add_field(name='role;help', value='Brings up this menu. Shocking, I know.', inline=False)
	response.add_field(name='role;greet', value='Shows a short introductory message.', inline=False)
	response.add_field(name='role;purgeServer', value='Removes from the database all information associated with the current server (Note: You do not need to run this before re-running role;countRoles. It is run automatically.', inline=False)

	#response.add_field(name='To Do List', value='- Add execution timer to role;countRoles.', inline=False)
	#response.set_footer(text='My code can be found [on GitLab](https://gitlab.com/CliffJameston/rolebot)')
	await ctx.send(embed=response)
	
# Just a brief greeting command
@bot.command(name='greet', help='Shows a brief greeting message.')
async def Greet(ctx):
	startMessage = discord.Embed(title='Greetings', description='I am Salazar. I am a small bot designed to get a list of all members on a server, count each of their roles, and provide those numbers to you, the user.\n\nI am written in Python, and my source code can be found [here on GitLab](https://gitlab.com/CliffJameston/rolebot).\n\nAs you are hopefully aware, I am written by @Cliff#4877. If you have any problems, complaints, or other feedback about me, feel free to let him know.\n\nThat is about all from me for now, feel free to type \'role;help\' to get started.', color=BOT_COLOR)
	await ctx.send(embed=startMessage)

# Individual role search command. 
@bot.command(name='show', help='Shows the number of users in a given role. TODO: Provide a paginated list of those users?')
async def ShowRole(ctx, roleToCount):
	# Gets the count for the given role and calculates some stats from it.
	foundRole, count = db_tools.GetCountFuzzy(roleToCount, ctx.guild.name)
	if db_tools.GetRoleCount(ctx.guild.name) > 0:
		users = db_tools.GetUserCount(ctx.guild.name)
		if users > 0:
			ratio = round((count/users)*100, 2)
		else:
			ratio = 0
		descriptionString = f'There are currently {count} out of {users} users with the {foundRole} role.\nThat is roughly {ratio}% of the total users.'

	# If role;countRoles hasn't been run, the database will be empty. Display a message to remind the user to do that first.
	else:
		descriptionString = f'There are currently no roles registered. Make sure to run role;countRoles first.'

	response = discord.Embed(title=foundRole, description=descriptionString, color=BOT_COLOR)
	await ctx.send(embed=response)
	return

# Shows all roles along with their user counts.
@bot.command(name='showAll', help='Shows the number of users with each role in the server.')
async def ShowAllRoleCounts(ctx):
	# Set variables for calls we need more than once
	roleCount = db_tools.GetRoleCount(ctx.guild.name)
	allRoles = db_tools.GetCountAll(ctx.guild.name)
	if roleCount == 0:
		# If we have no roles to show, tell the user how to count them
		descriptionString = f'There are currently no roles registered. Make sure to run role;countRoles first.'
		response = discord.Embed(title='No Roles', description=descriptionString, color=BOT_COLOR)
		await ctx.send(embed=response)
		return
	else:
		descriptionString = f'There are currently {db_tools.GetUserCount(ctx.guild.name)} users, with {roleCount} unique roles. These are the roles:'

	# Set up pagination variables
	pages = []
	paginationLimit = 20 # Change this to adjust the number of roles displayed per page
	pageMax = math.ceil(len(allRoles)/paginationLimit)

	# Create page embeds
	for page in range(0, pageMax):
		# Create single page embed
		embed = discord.Embed(title='All Roles', description=descriptionString, color=BOT_COLOR)
		valueString=''
		# Loop through {paginationLimit} rows in allRoles, adding the Name and Count to the bottom of the value we will be adding to the embed.
		for i in range(0, paginationLimit):
			try:
				row = allRoles[i + (paginationLimit * page)]
				rowValue = f'{row[0]} - {row[1]}'
				valueString = valueString + '\n' + rowValue
			except:
				# If there are no more rows, break out of the loop
				break
		# Add the values we just formatted to the page, and add the page to the list of pages
		embed.add_field(name='Roles', value=valueString)
		pages.append(embed)

	# Set up DiscordUtils' Paginator object and display it
	paginator = DiscordUtils.Pagination.CustomEmbedPaginator(ctx, timeout=60, auto_footer=True, remove_reactions=True)
	paginator.add_reaction('⏮️', "first")
	paginator.add_reaction('⏪', "back")
	paginator.add_reaction('⏩', "next")
	paginator.add_reaction('⏭️', "last")
	await paginator.run(pages)
	return

# Counts roles.
@bot.command(name='countRoles', help='Gets every user in the server and tallies all their roles. TODO: Add execution time.')
async def CountAllRoles(ctx):
	# Since guild.fetch_members can possibly take a while, display a message to let the user know things are happening.
	startMessage = discord.Embed(title='Counting Roles', description='This may take some time. I will inform you when I have finished.', color=BOT_COLOR)
	await ctx.send(embed=startMessage)
	
	# Clear the old role counts
	db_tools.PurgeGuild(ctx.guild.name)

	fetchedMembers = []
	# Process the roles for each member.
	async for member in ctx.guild.fetch_members(limit=None):
		# Do all the processing here and just send strings over, that way I don't have to worry about data types.
		memberRoles = []
		for role in member.roles:
			memberRoles.append(role.name)
		fetchedMembers.append([member.name, member.id, memberRoles, ctx.guild.name])

	db_tools.ProcessMembers(fetchedMembers)

	
	# Display a message to announce that we finished counting.
	finishedMessage = discord.Embed(title='Counting Finished', description=f'I have finished counting all roles for {db_tools.GetUserCount(ctx.guild.name)} users. Use role;show {{role_name}} or role;showAll to have me display the counts.', color=BOT_COLOR)
	await ctx.send(embed=finishedMessage)
	return

@bot.command(name='purgeServer', help='Deletes all entries for this server from the database.')
async def PurgeServer(ctx):
	db_tools.PurgeGuild(ctx.guild.name)
	startMessage = discord.Embed(title='Roles Purged', description=f'Succesfully removed all entries for the server "{ctx.guild.name}" from the database.', color=BOT_COLOR)
	await ctx.send(embed=startMessage)

@bot.command(name='ping', help='Pings me.')
async def Ping(ctx):
	await ctx.send('I am here.')

db_tools.OnStartup()
print('Logging into Discord.')
bot.run(TOKEN)