# RoleBot

A Discord bot to count every role in a given server, and count how many users have each role.

## Prefix

RoleBot can be summoned using the prefixes 'role;' or 'role!', and will also respond if you @ it with a command.

## Commands

- countRoles - The first command that should be run after install. This actually gets the list of users in the server and counts up all their roles.
- help - Shows the bot's help page. Essentially a less verbose version of this page.
- show [role] - Searches (fuzzy) for a given string in all roles for the server. If more than one role is found, only the first is returned currently. I may add a paginated list of users at some point, not sure.
- showAll - Shows a list of all roles in a server, along with the count of how many users have that role.
- ping - Pings RoleBot
- greet - Introduces the bot to the channel it's in.
- purgeServer - removes from the database all information associated with the current server.

## Privacy

This bot uses the privileged Members intent of the Discord API. It saves the following information to a SQLite database, in order to increase performance. No other information is stored.
- User Name
- User ID
- User Roles
- Server (Guild)

## Requirements

If running this bot yourself, you will need to do a little setup first. 

This bot relies on the following packages to run:
- discord.py
- python-dotenv
- DiscordUtils

It also requires a file named .env that contains the token for your instance of the bot saved with the key DISCORD_TOKEN

