import os
import sqlite3
import json
from dotenv import load_dotenv


DB_PATH = os.path.join(os.path.dirname(__file__), 'roles.db')

# Startup and Setup stuff

def ConnectDB():
	dbconnection = sqlite3.connect(DB_PATH)
	return dbconnection

def GetCursor():
	cursor = ConnectDB().cursor()
	return cursor

def SetupDB():
	cur = GetCursor()
	
	roles_sql = """
	CREATE TABLE IF NOT EXISTS roles (
	role_id text PRIMARY KEY,
	role text NOT NULL,
	count integer NOT NULL,
	guild text NOT NULL)"""
	cur.execute(roles_sql)

	users_sql = """
	CREATE TABLE IF NOT EXISTS users (
	name_id text PRIMARY KEY,
	name text NOT NULL,
	id text NOT NULL,
	roles text NOT NULL,
	guild text NOT NULL)"""
	cur.execute(users_sql)

def IDGen(name, guild):
	returnID = str(name) + " - " + str(guild)
	return returnID

def GetCount(role, guild, cursor):
	# Try to find role in roles and return count. If role not found, return 0
	try:
		cursor.execute('SELECT count FROM roles WHERE role_id=?', (IDGen(role, guild),))
		result = cursor.fetchone()[0]
		return result
	except:
		return 0
	return 0

def GetCountFuzzy(role, guild):
	# Try to find role in roles. Searches fuzzy and returns the full name of the role, as well as the count
	cur = GetCursor()
	fuzz = '%' + role + '%'
	try:
		cur.execute('SELECT * FROM roles WHERE role LIKE ? AND guild=?', (fuzz, guild))
		result = cur.fetchone()
		resultRole = result[1]
		resultCount = result[2]
		return resultRole, resultCount;
	except:
		return 'No Role Found', 0;
	return 'No Role Found', 0;

def GetCountAll(guild):
	# Returns a list of all roles and their counts
	cur = GetCursor()
	cur.execute("SELECT role, count FROM roles WHERE guild=? ORDER BY count DESC", (guild,))
	allRoles = cur.fetchall()
	return allRoles

def PurgeGuild(guild):
	con = ConnectDB()
	cur = con.cursor()
	cur.execute('DELETE FROM users WHERE guild=?', (guild,))
	cur.execute('DELETE FROM roles WHERE guild=?', (guild,))
	con.commit()

def ProcessMembers(members):
	connection = ConnectDB()
	cursor = connection.cursor()
	for member in members:
		# Member list items are {name, ID, roles, guild}
		AddUser(member[0], member[1], member[2], member[3], cursor)
		AddUpdateRole(member[2], member[3], cursor)
	connection.commit()
	return

def AddUser(memberName, memberID, memberRoles, guild, cursor):
	command_string = 'REPLACE INTO users(name_id, name, id, roles, guild) VALUES (?,?,?,?,?)'
	cursor.execute(command_string, (IDGen(memberID, guild), memberName, memberID, json.dumps(memberRoles), guild))

def AddUpdateRole(roles, guild, cursor):
	command_string = 'REPLACE INTO roles(role_id, role, count, guild) VALUES (?,?,?,?)'
	for role in roles:
		roleCount = GetCount(role, guild, cursor) + 1
		cursor.execute(command_string, (IDGen(role, guild), role, roleCount, guild))

def OnStartup():
	print('Setting up Database.')
	SetupDB()

def GetUserCount(guild):
	cur = GetCursor()
	cur.execute("SELECT * FROM users WHERE guild=?", (guild,))
	return len(cur.fetchall())

def GetRoleCount(guild):
	cur = GetCursor()
	cur.execute("SELECT * FROM roles WHERE guild=?", (guild,))
	return len(cur.fetchall())

def ListUsers(guild):
	cur = GetCursor()
	allUsers = cur.execute('SELECT * FROM users WHERE guild=?', (guild,))
	return allUsers